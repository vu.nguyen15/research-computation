import React, { useMemo, useState } from "react";
import { AgGridReact } from "ag-grid-react";
import testData from "../../data/testData";

const filterParams = {
  comparator: (filterLocalDateAtMidnight, cellValue) => {
    const dateAsString = cellValue;
    if (dateAsString == null) return -1;
    const dateParts = dateAsString.split("/");
    const cellDate = new Date(
      Number(dateParts[2]),
      Number(dateParts[1]) - 1,
      Number(dateParts[0])
    );
    if (filterLocalDateAtMidnight.getTime() === cellDate.getTime()) {
      return 0;
    }
    if (cellDate < filterLocalDateAtMidnight) {
      return -1;
    }
    if (cellDate > filterLocalDateAtMidnight) {
      return 1;
    }
    return 0;
  },
  browserDatePicker: true,
};

const columnDefs = [
  { field: "athlete", rowDrag: true },
  { field: "age", filter: "agNumberColumnFilter", maxWidth: 100 },
  { field: "country" },
  { field: "year", maxWidth: 100 },
  {
    field: "date",
    filter: "agDateColumnFilter",
    filterParams: filterParams,
  },
  { field: "sport" },
  { field: "gold", filter: "agNumberColumnFilter" },
  { field: "silver", filter: "agNumberColumnFilter" },
  { field: "bronze", filter: "agNumberColumnFilter" },
  { field: "total", filter: false },
];

const defaultColDef = {
  flex: 1,
  minWidth: 150,
  filter: true,
};

const Grid = () => {
  const [rowData, setRowData] = useState(testData);

  return (
    <div className="ag-theme-alpine" style={{ height: 1000, width: 800 }}>
      <AgGridReact
        rowData={rowData}
        columnDefs={columnDefs}
        defaultColDef={defaultColDef}
        rowDragManaged={true}
        animateRows={true}
      ></AgGridReact>
    </div>
  );
};

export default Grid;
