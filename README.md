# PouchDB

 - Cant sync with DB that outside ecosystem of CouchDB (which mean cant sync with mongo/mysql/non-couchdb).

 - [Solution 1: Acebase (support sync with MongoDB)](https://github.com/appy-one/acebase);

 - [Solution 2: IDB](https://github.com/jakearchibald/idb)

 - Document is quite simple. Hard to dig deeper into how it works.


# 1. Storage limit: [Source](https://pouchdb.com/faq.html)

 - [Chrome](https://developer.chrome.com/docs/apps/offline_storage/#temporary): 1/3 available diskspace (80% now)
 - [Firefox](https://developer.mozilla.org/en-US/docs/Web/API/IndexedDB_API/Browser_storage_limits_and_eviction_criteria#storage_limits): first 50MB then will pop modal to confirm use more (50% diskspace now)
 - Safari: up to 500MB [ref1](https://trac.webkit.org/changeset/237700/webkit/), 1GB then prompt after 200mb more(not confirmed) [ref2](https://web.dev/storage-for-the-web/)
 - [MS Edge](https://twitter.com/gregwhitworth/status/1020391736974094336): 50MB - 20GB
 - Mobile: 50 - up to 200MB depends on browser
 - Mordern browser: [link](https://web.dev/storage-for-the-web/)


# 2. Table Libs comparision

|            | Tanstack                      | AG Grid     |
| ---------- | ----------------------------- | ----------- |
| Pros       | Free                          | EZ customize CSS, also feature    |
|            | Support multiple frameworks   | Config based syntax, Ez to use    |
|            | EZ to customize CSS           | Fast render cuz use freestyle div |
|            | Support basic features        | Support Drag n drop               |
|            |                               | Has features that support high freq update table |
|            |                               | Support export excel, also some excel feature on UI |
| ---------- | ----------------------------- | --------------------------------- |
| Cons       | Bad syntax, code base         | Money money money                 |
|            | Super slow on big database    | |
|            | Docs is really bad and lag    | |


## Benchmark:
 
+ 90k entries

| Tanstack | AG Grid |
| ------ | ------ |
| minimum 10s to interact with UI, sometime freeze       |   approx 1600ms     |


+ 900k entries

| Tanstack | AG Grid |
| ------ | ------ |
| Crash app   |   approx 3400ms     |


# 3. Chart Lib Comparision

| Apexchart | ChartJs | Fusionchart|
| ------ | ------ |---------------|
| | | |

# 4. DB

| Dexie | IDB | Acebase|
| ------ | ------ |---------------|
| | | |
